{-# LANGUAGE OverloadedStrings #-}

module Main (main) where

import Data.Acid (openLocalState)
import Data.Acid.Remote (acidServer, skipAuthenticationCheck)
import Data.Maybe (fromMaybe)
import Fun.With.Scotty.State (AppState (AppState))
import Network (PortID (PortNumber))
import System.Environment (getEnvironment)

main :: IO ()
main = do
	env <- getEnvironment
	let port = PortNumber $ fromIntegral
		(read . fromMaybe "5000" $ lookup "ACID_PORT" env :: Int)
	acid <- openLocalState (AppState "")
	putStrLn $ "Started backend on port " ++ show port ++ ". Press Ctrl-C to exit."
	acidServer skipAuthenticationCheck port acid
