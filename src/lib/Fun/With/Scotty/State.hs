{-# LANGUAGE DeriveDataTypeable, TemplateHaskell, TypeFamilies #-}

module Fun.With.Scotty.State (AppState (AppState), QueryState (..), UpdateState (..)) where

import Control.Monad.Reader (ask)
import Control.Monad.State (put)
import Data.Acid (Query, Update, makeAcidic)
import Data.SafeCopy (base, deriveSafeCopy)
import Data.Text.Lazy (Text)
import Data.Typeable (Typeable)

data AppState = AppState Text
	deriving (Show, Typeable)

queryState :: Query AppState Text
queryState = ask >>= \(AppState state) -> return state

updateState :: Text -> Update AppState ()
updateState state = put (AppState state)

$(deriveSafeCopy 0 'base ''AppState)

$(makeAcidic ''AppState ['queryState, 'updateState])
