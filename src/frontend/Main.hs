{-# LANGUAGE OverloadedStrings #-}

import Control.Monad (void)
import Control.Monad.Trans (lift)
import Data.Acid (query, update)
import Data.Acid.Remote (openRemoteState, skipAuthenticationPerform)
import Data.Maybe (fromMaybe)
import Fun.With.Scotty.State (QueryState (QueryState), UpdateState (UpdateState))
import Network (PortID (PortNumber))
import System.Environment (getEnvironment)
import Web.Scotty (get, param, post, redirect, scotty, text)


main :: IO ()
main = do
	env <- getEnvironment
	let port = read . fromMaybe "3000" $ lookup "PORT" env
	let acidHost = fromMaybe "localhost" $ lookup "ACID_HOST" env
	let acidPort = PortNumber $ fromIntegral
		(read . fromMaybe "5000" $ lookup "ACID_PORT" env :: Int)
	acid <- openRemoteState skipAuthenticationPerform acidHost acidPort
	scotty port $ do
		get "/" $ do
			state <- lift $ query acid QueryState
			text state
		post "/" $ do
			newstate <- param "state"
			void . lift $ update acid (UpdateState newstate)
			redirect "/"
